package routes

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/mauromamani20014/react-go/server/controllers"
	"gitlab.com/mauromamani20014/react-go/server/middlewares"
)

func Setup(app *fiber.App) {
  app.Post("/api/v1/auth/register", controllers.Register)
  app.Post("/api/v1/auth/login", controllers.Login)

  // protected routes
  app.Use(middlewares.IsAuthenticated)

  // Auth
  app.Get("/api/v1/auth/whoami", controllers.GetUser)
  app.Post("/api/v1/auth/logout", controllers.Logout)

  app.Put("/api/v1/user/change-password", controllers.UpdatePassword)
  app.Put("/api/v1/user/change-info", controllers.UpdateInfo)

  // User
  app.Get("/api/v1/user/all", controllers.FindAll)
  app.Get("/api/v1/user/:id", controllers.FindOne)
  app.Put("/api/v1/user/:id", controllers.UpdateUser)
  app.Delete("/api/v1/user/:id", controllers.DeleteUser)
  app.Post("/api/v1/user/create", controllers.CreateUser)

  // products
  app.Get("/api/v1/product/all", controllers.FindAllProducts)
  app.Get("/api/v1/product/:id", controllers.FindOneProduct)
  app.Put("/api/v1/product/:id", controllers.UpdateProduct)
  app.Delete("/api/v1/product/:id", controllers.DeleteProduct)
  app.Post("/api/v1/product/create", controllers.CreateProduct)

  // Roles
  app.Get("/api/v1/roles/all", controllers.AllRoles)
  app.Get("/api/v1/roles/:id", controllers.FindOneRole)
  app.Put("/api/v1/roles/:id", controllers.UpdateRole)
  app.Delete("/api/v1/roles/:id", controllers.DeleteRole)
  app.Post("/api/v1/roles/create", controllers.CreateRole)

  app.Post("/api/v1/upload", controllers.Upload)

  // Permissions
  app.Get("/api/v1/permissions/all", controllers.AllPermissions)
  app.Static("/api/v1/upload", "./uploads")

  app.Get("/api/v1/orders/all", controllers.FindAllOrders)
}
