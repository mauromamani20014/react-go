package controllers

import "github.com/gofiber/fiber/v2"

func Upload(c *fiber.Ctx) error {
  form, err := c.MultipartForm()
  if err != nil {
    return err
  }

  // image: nombre
  files := form.File["image"]

  var filename string
  for _, file := range files {
    filename = file.Filename

    if err := c.SaveFile(file, "./uploads/" + filename); err != nil {
      return err
    }
  }

  return c.JSON(fiber.Map{
    "url": "http://localhost:8000/api/v1/upload/" + filename,
  })
}
