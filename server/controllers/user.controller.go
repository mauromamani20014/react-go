package controllers

import (
	"math"
	"strconv"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/mauromamani20014/react-go/server/database"
	"gitlab.com/mauromamani20014/react-go/server/entities"
)

func FindAll(c *fiber.Ctx) (error) {
  // pagination
  page, _ := strconv.Atoi(c.Query("page", "1"))
  limit := 3
  offset := (page - 1) * limit
  // total
  var total int64

  var users []entities.User

  database.DB.Preload("Role").Offset(offset).Limit(limit).Find(&users)
  database.DB.Model(&entities.User{}).Count(&total)

  return c.JSON(fiber.Map{
    "data": users,
    "meta": fiber.Map{
      "page":      page,
      "total":     total,
      "last_page": math.Ceil(float64(int(total) / limit)),
    },
  })
}

func CreateUser(c *fiber.Ctx) (error) {
  var user entities.User

  if err := c.BodyParser(&user); err != nil {
    return err
  }

  // default password
  user.HashPassword("12345678")

  database.DB.Preload("Role").Create(&user)

  return c.JSON(user)
}


func FindOne(c *fiber.Ctx) (error) {
  uid := c.Params("id")
  var user entities.User

  database.DB.Preload("Role").Where("id = ?", uid).First(&user)
  if user.Email == "" {
    c.Status(fiber.StatusNotFound)
    return c.JSON(fiber.Map{
      "message": "User not found!",
    })

  }

  return c.JSON(user)
}


func UpdateUser(c *fiber.Ctx) (error) {
  uid := c.Params("id")
  var user entities.User

  database.DB.Where("id = ?", uid).First(&user)
  if user.Email == "" {
    c.Status(fiber.StatusNotFound)
    return c.JSON(fiber.Map{
      "message": "User not found!",
    })

  }

  if err := c.BodyParser(&user); err != nil {
    return err
  }

  database.DB.Model(&user).Updates(user)

  return c.JSON(user)
}

func DeleteUser(c *fiber.Ctx) (error) {
  uid := c.Params("id")
  var user entities.User

  database.DB.Where("id = ?", uid).First(&user)
  if user.Email == "" {
    c.Status(fiber.StatusNotFound)
    return c.JSON(fiber.Map{
      "message": "User not found!",
    })

  }

  database.DB.Delete(&user)


  return c.JSON(fiber.Map{
    "message" : "User deleted successfully",
  })
}

