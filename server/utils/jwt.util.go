package utils

import (
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

const SECRET_KEY = "SECRET_@123"

func GenerateJwt(issuer string) (string, error) {
  expireTime := time.Now().Add(time.Hour * 24)
  claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
    Issuer    : issuer,
    ExpiresAt : expireTime.Unix(), // Expires 1 day
  })

  return claims.SignedString([]byte(SECRET_KEY))
}

func ParseJwt(cookie string) (string, error) {
  token, err := jwt.ParseWithClaims(cookie, &jwt.StandardClaims{}, func (token *jwt.Token) (interface{}, error) {
    return []byte(SECRET_KEY), nil
  })

  if err != nil || !token.Valid {
    return "", err
  }

  claims := token.Claims.(*jwt.StandardClaims)

  return claims.Issuer, nil
}
