package controllers

import (
	"net/http"
	"strconv"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"

	"gitlab.com/mauromamani20014/react-go/server/database"
	"gitlab.com/mauromamani20014/react-go/server/entities"
	"gitlab.com/mauromamani20014/react-go/server/utils"
)

func Register(c *fiber.Ctx) error {
  var body map[string]string

  if err := c.BodyParser(&body); err != nil {
    return err
  }

  if body["password"] != body["password_confirm"] {
    c.Status(http.StatusBadRequest)
    return c.JSON(fiber.Map{
      "message": "Passwords don´t match!",
    })
  }

  user := entities.NewUser(
    body["first_name"],
    body["last_name"],
    body["email"],
  )
  user.HashPassword(body["password"])

  database.DB.Create(&user)

  return c.JSON(user)
}

func Login(c *fiber.Ctx) error {
  var body map[string]string

  if err := c.BodyParser(&body); err != nil {
    return err
  }

  var user entities.User

  database.DB.Where("email = ?", body["email"]).First(&user)
  if user.Email == "" {
    c.Status(http.StatusNotFound)
    return c.JSON(fiber.Map{
      "message": "User not found!",
    })
  }

  if err := user.ComparePassword(body["password"]); err != nil {
    c.Status(http.StatusForbidden)
    return c.JSON(fiber.Map{
      "message": "Password dont match!",
    })
  }

  token, err := utils.GenerateJwt(strconv.Itoa(int(user.Id)))
  if err != nil {
    return c.SendStatus(fiber.StatusInternalServerError)
  }

  // cookie
  cookie := fiber.Cookie{
    Name     : "jwt",
    Value    : token,
    Expires  : time.Now().Add(time.Hour * 24),
    HTTPOnly : true,
  }
  c.Cookie(&cookie) // Seteando cookie

  return c.JSON(fiber.Map{
    "user" : user,
    "ok"   : true,
  })
}

type Claims struct {
  jwt.StandardClaims
}

func GetUser(c *fiber.Ctx) error {
  // get cookie
  cookie := c.Cookies("jwt")

  // parse token
  uid, err := utils.ParseJwt(cookie)

  if err != nil {
    c.Status(http.StatusForbidden)
    return c.JSON(fiber.Map{
      "message": "unauthenticated!",
    })
  }

  var user entities.User
  database.DB.Where("id = ?", uid).First(&user)

  return c.JSON(user)
}

func Logout(c *fiber.Ctx) (error) {
  // la cookie no se puede eliminar, la reemplazamos con un que ya este expirada
  cookie := fiber.Cookie{
    Name     : "jwt",
    Value    : "",
    Expires  : time.Now().Add(-time.Hour),
    HTTPOnly : true,
  }

  c.Cookie(&cookie)

  return c.JSON(fiber.Map{
    "message": "success",
  })
}

func UpdateInfo(c *fiber.Ctx) (error) {
  var body map[string]string

  if err := c.BodyParser(&body); err != nil {
    return err
  }

  cookie := c.Cookies("jwt")

  // parse token
  uid, err := utils.ParseJwt(cookie)

  if err != nil {
    c.Status(http.StatusForbidden)
    return c.JSON(fiber.Map{
      "message": "unauthenticated!",
    })
  }

  var user entities.User
  database.DB.Model(&user).Where("id = ?", uid).Updates(body)

  return c.JSON(user)
}

func UpdatePassword(c *fiber.Ctx) (error) {
  var body map[string]string

  if err := c.BodyParser(&body); err != nil {
    return err
  }

  if body["password"] != body["password_confirm"] {
    c.Status(http.StatusBadRequest)
    return c.JSON(fiber.Map{
      "message": "Passwords don´t match!",
    })
  }

  cookie := c.Cookies("jwt")

  // parse token
  uid, err := utils.ParseJwt(cookie)

  if err != nil {
    c.Status(http.StatusForbidden)
    return c.JSON(fiber.Map{
      "message": "unauthenticated!",
    })
  }

  user := entities.User{}
  user.HashPassword(body["password"])

  database.DB.Model(&user).Where("id = ?", uid).Updates(user)

  return c.JSON(user)
}
