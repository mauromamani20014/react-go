package entities

import "golang.org/x/crypto/bcrypt"

type User struct {
  Id        uint   `json:"id"`
  FirstName string `json:"first_name"`
  LastName  string `json:"last_name"`
  Email     string `json:"email"     gorm:"unique"`
  Password  []byte `json:"-"`
  // relation
  RoleId    uint   `json:"role_id"`
  Role      Role   `json:"role"      gorm:"foreignKey:RoleId"`
}

func (u *User) HashPassword(password string) {
  hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
  u.Password = hashedPassword
}

func (u *User) ComparePassword(password string) (error) {
  return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
}

func NewUser(firstName string, lastName string, email string) (*User) {
  return &User{
    FirstName : firstName,
    LastName  : lastName,
    Email     : email,
  }
}
