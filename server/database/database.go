package database

import (
	"fmt"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"gitlab.com/mauromamani20014/react-go/server/entities"
)

var DB *gorm.DB

func Connect() {
  db, err := gorm.Open(sqlite.Open("sqlite.db"), &gorm.Config{})
  if err != nil {
    panic("Could not connect to the database!")
  }

  db.AutoMigrate(
    &entities.User{},
    &entities.Role{},
    &entities.Permission{},
    &entities.Product{},
    &entities.Order{},
    &entities.OrderItem{},
  )
  DB = db

  fmt.Println("Database running")
}

