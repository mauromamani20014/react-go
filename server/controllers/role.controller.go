package controllers

import (
	"fmt"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/mauromamani20014/react-go/server/database"
	"gitlab.com/mauromamani20014/react-go/server/entities"
)

func AllRoles(c *fiber.Ctx) (error) {
  var roles []entities.Role

  database.DB.Find(&roles)

  return c.JSON(roles)
}

func CreateRole(c *fiber.Ctx) (error) {
  var createRoleDto fiber.Map

  if err := c.BodyParser(&createRoleDto); err != nil {
    return err
  }

  list := createRoleDto["permissions"].([]interface{})
  permissions := make([]entities.Permission, len(list))

  // populate permissions
  for idx, permissionsId := range list {
    id, _ := strconv.Atoi(permissionsId.(string))

    permissions[idx] = entities.Permission{Id: uint(id)}
  }
  fmt.Println(permissions)
  role := entities.Role{Name: createRoleDto["name"].(string), Permission: permissions}

  database.DB.Create(&role)

  return c.JSON(role)
}


func FindOneRole(c *fiber.Ctx) (error) {
  id := c.Params("id")
  var role entities.Role

  database.DB.Preload("permissions").Where("id = ?", id).First(&role)
  if role.Name == "" {
    c.Status(fiber.StatusNotFound)
    return c.JSON(fiber.Map{
      "message": "Role not found!",
    })

  }

  return c.JSON(role)
}


func UpdateRole(c *fiber.Ctx) (error) {
  id := c.Params("id")

  var updateRoleDto fiber.Map
  if err := c.BodyParser(&updateRoleDto); err != nil {
    return err
  }

  list := updateRoleDto["permissions"].([]interface{})
  permissions := make([]entities.Permission, len(list))

  // populate permissions
  for idx, permissionsId := range list {
    id, _ := strconv.Atoi(permissionsId.(string))

    permissions[idx] = entities.Permission{Id: uint(id)}
  }

  var result interface{}
  database.DB.Table("role_permissions").Where("role_id", id).Delete(&result)

  roleId, _ := strconv.Atoi(id)
  role := entities.Role{
    Id:         uint(roleId),
    Name:       updateRoleDto["name"].(string),
    Permission: permissions,
  }

  database.DB.Model(&role).Updates(role)

  return c.JSON(role)
}

func DeleteRole(c *fiber.Ctx) (error) {
  id := c.Params("id")
  var role entities.Role

  database.DB.Where("id = ?", id).First(&role)
  if role.Name == "" {
    c.Status(fiber.StatusNotFound)
    return c.JSON(fiber.Map{
      "message": "Role not found!",
    })

  }

  database.DB.Delete(&role)


  return c.JSON(fiber.Map{
    "message" : "Role deleted successfully",
  })
}

