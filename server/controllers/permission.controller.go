package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mauromamani20014/react-go/server/database"
	"gitlab.com/mauromamani20014/react-go/server/entities"
)

func AllPermissions(c *fiber.Ctx) (error) {
  var permissions []entities.Permission

  database.DB.Find(&permissions)

  return c.JSON(permissions)
}
