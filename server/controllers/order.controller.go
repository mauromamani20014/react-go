package controllers

import (
	"math"
	"strconv"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/mauromamani20014/react-go/server/database"
	"gitlab.com/mauromamani20014/react-go/server/entities"
)

func FindAllOrders(c *fiber.Ctx) (error) {
  // pagination
  page, _ := strconv.Atoi(c.Query("page", "1"))
  limit := 3
  offset := (page - 1) * limit
  // total
  var total int64

  var orders []entities.Order

  database.DB.Offset(offset).Limit(limit).Find(&orders)
  database.DB.Model(&entities.Order{}).Count(&total)

  return c.JSON(fiber.Map{
    "data": orders,
    "meta": fiber.Map{
      "page":      page,
      "total":     total,
      "last_page": math.Ceil(float64(int(total) / limit)),
    },
  })
}
