package controllers

import (
	"math"
	"strconv"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/mauromamani20014/react-go/server/database"
	"gitlab.com/mauromamani20014/react-go/server/entities"
)

func FindAllProducts(c *fiber.Ctx) (error) {
  // pagination
  page, _ := strconv.Atoi(c.Query("page", "1"))
  limit := 3
  offset := (page - 1) * limit
  // total
  var total int64

  var products []entities.Product

  database.DB.Offset(offset).Limit(limit).Find(&products)
  database.DB.Model(&entities.Product{}).Count(&total)

  return c.JSON(fiber.Map{
    "data": products,
    "meta": fiber.Map{
      "page":      page,
      "total":     total,
      "last_page": math.Ceil(float64(int(total) / limit)),
    },
  })
}

func CreateProduct(c *fiber.Ctx) (error) {
  var product entities.Product

  if err := c.BodyParser(&product); err != nil {
    return err
  }

  database.DB.Create(&product)

  return c.JSON(product)
}


func FindOneProduct(c *fiber.Ctx) (error) {
  uid := c.Params("id")
  var product entities.Product

  database.DB.Where("id = ?", uid).First(&product)
  if product.Title == "" {
    c.Status(fiber.StatusNotFound)
    return c.JSON(fiber.Map{
      "message": "Product not found!",
    })

  }

  return c.JSON(product)
}


func UpdateProduct(c *fiber.Ctx) (error) {
  uid := c.Params("id")
  var product entities.Product

  database.DB.Where("id = ?", uid).First(&product)
  if product.Title == "" {
    c.Status(fiber.StatusNotFound)
    return c.JSON(fiber.Map{
      "message": "Product not found!",
    })

  }

  if err := c.BodyParser(&product); err != nil {
    return err
  }

  database.DB.Model(&product).Updates(product)

  return c.JSON(product)
}

func DeleteProduct(c *fiber.Ctx) (error) {
  uid := c.Params("id")
  var product entities.Product

  database.DB.Where("id = ?", uid).First(&product)
  if product.Title == "" {
    c.Status(fiber.StatusNotFound)
    return c.JSON(fiber.Map{
      "message": "Product not found!",
    })

  }

  database.DB.Delete(&product)


  return c.JSON(fiber.Map{
    "message" : "Product deleted successfully",
  })
}

