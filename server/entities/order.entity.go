package entities

import "gorm.io/gorm"

type Order struct {
  gorm.Model
  Name      string      `json:"-"        gorm:"-"` // se deben juntar first_name + last_name
  FirstName string      `json:"first_name"`
  LastName  string      `json:"last_name"`
  Email     string      `json:"email"`
  OrderItem []OrderItem `json:"order_items" gorm:"foreignKey:OrderId"`
}

type OrderItem struct {
  gorm.Model
  OrderId      uint    `json:"order_id"`
  ProductTitle string  `json:"product_title"`
  Price        float32 `json:"price"`
  Quantity     uint    `json:"quantity"`
}
