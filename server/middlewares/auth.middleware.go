package middlewares

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mauromamani20014/react-go/server/utils"
)

func IsAuthenticated(c *fiber.Ctx) (error) {
  cookie := c.Cookies("jwt")

  if _, err := utils.ParseJwt(cookie); err != nil {
    c.Status(fiber.StatusUnauthorized)
    return c.JSON(fiber.Map{
      "message": "unauthenticated!",
    })
  }

  return c.Next()
}
